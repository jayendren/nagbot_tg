#!/usr/bin/env ruby

require 'rubygems'
require 'logger'
require 'uri'
require 'net/https'
require 'facter'

# global variables
# nagios
@nagios_fifo = "/var/spool/nagios/rw/ndbot.fifo"         # path to nagios named pipe
@log         = "/var/spool/nagios/nagbot_tg.log"         # path to logfile for this daemon
@log_size    = 1024000                                   # logfile size in kb
@logger      = Logger.new(@log, @log_size)               # logger instance

@tg_prot     = "https"                                   
@tg_host     = "api.telegram.org"                    
@tg_botid    = "bot123"       # https://core.telegram.org/bots/api
@tg_api      = "SendMessage?"
@tg_chatid   = "-1234"        # name of chat group
@tg_url      = "#{@tg_prot}://#{@tg_host}/#{@tg_botid}/#{@tg_api}"
@tg_timeout  = 600
@tg_retries  = 5 
@nagios_fqdn = Facter.value("fqdn")

def nagbot_tg_post(url,message)

  begin
    uri                = URI.parse("#{url}")
    http               = Net::HTTP.new(uri.host, uri.port)
    http.read_timeout  = @tg_timeout

    if url.to_s.match(/^http\:\/\//)
      http.use_ssl     = false
    else
      http.use_ssl     = true
      http.verify_mode = OpenSSL::SSL::VERIFY_NONE
    end    

    http.start {|http|
      request        = Net::HTTP::Get.new(uri.request_uri)
      request.body   = "#{message}"
      @tg_response   = http.request(request)
    }

  rescue => e
    @logger.error e.message
    system("echo '#{e.message} - #{@tg_url}' #{@mail_cmd} > /dev/null 2>&1")
    e.backtrace.each { |line| 
      @logger.error line       
    }
    sleep 3
    retry if (@tg_retries -= 1) > 0
  end

end

# nagbot_tg daemon 
nagbot_tg = Proc.new do
  while true
    @spool = open(@nagios_fifo, "r+")     
    @alert = @spool.gets  

    begin
    rescue => e
      @logger.error e.message
      system("echo '#{e.message} - #{@nagios_fifo}' #{@mail_cmd} > /dev/null 2>&1")
      e.backtrace.each { |line| 
        @logger.error line         
      }
      exit 1
    end

    unless ! (@alert.to_s.match(/OK|UP|WARN|UNKNOWN|CRIT|DOWN/)) # add alert types here

      # set the icon for alert    
      if @alert.to_s.match(/OK|UP/) 
        #@state = "🔵"
        @state = "😄"
      end
      if @alert.to_s.match(/WARN|UNKNOWN/)
        #@state = "🔴"
        @state = "😳"
      end      
      if @alert.to_s.match(/CRIT/)
        #@state = "🔴"
        @state = "😡"
      end
      if @alert.to_s.match(/DOWN/)
        #@state = "🔴"
        @state = "😡"
      end    

      begin
        @tg_post = URI.encode("chat_id=#{@tg_chatid}&text=#{@nagios_fqdn} #{@state} #{@alert}")
      rescue => e
        @logger.error e.message
        e.backtrace.each { |line| @logger.error line }
      end  

      @logger.info "alert           : #{@alert}"
      @logger.info "posting         : #{@tg_url}#{@tg_post}"

      # tg post:
      nagbot_tg_post(@tg_url,@tg_post)
      @logger.info "reply           : #{@tg_response}"

    else
      @logger = Logger.new(@log, @log_size)
      @logger.info "ignoring: #{@alert}"
    end # unless

    sleep 1

    @spool.close

  end # while

end # Proc

fork { nagbot_tg.call }