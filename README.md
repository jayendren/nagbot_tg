# Table of contents

* [About](#about)
* [Requirements](#requirements)
* [Troubleshooting](#troubleshooting)

### About:

Ruby daemon to post nagios alerts to Telegram
Implementation based Traiano Welcomes perl/xmpp nagbot.

![](demo/nagbot_tg.png)

### Requirements:
#### Ruby:

* Ruby
* rubygems
* logger
* uri
* net/https
* facter

#### Nagios:
* nagios server needs to be configured to write alerts to a named pipe/fifo in:
* nagios/etc/check_commands.cfg:

  `define command {  
       command_name notify-service-by-fifo 
       command_line /bin/echo "$HOSTNAME$ $NOTIFICATIONTYPE$ $SERVICEOUTPUT$ $SERVICEDESC$ $SERVICESTATE$ $LONGDATETIME$" > /var/spool/nagios/rw/ndbot.fifo
  }`

  `define command {  
       command_name                    notify-host-by-fifo
       command_line                    /bin/echo "$HOSTNAME$ $NOTIFICATIONTYPE$ $HOSTADDRESS$ $HOSTSTATE$ $HOSTOUTPUT$ $LONGDATETIME$" > /var/spool/nagios/rw/ndbot.fifo
  }`

* nagios/etc/nagios.cfg:  

  `global_host_event_handler=notify-host-by-fifo`
  `global_service_event_handler=notify-service-by-fifo`

* Create the fifo with:

   `mkfifo /var/spool/nagios/rw/ndbot.fifo`

#### FreeBSD:
* Copy of the rc_nagbot_tg script as:
  `/usr/local/etc/rc.d/nagbot_tg`
with executable permissions
* /etc/rc.conf entry:
  `nagbot_tg_enable="YES"`
* Copy of the ruby daemon (nagbot_tg.rb) as:
  `/usr/local/sbin/nagbot_tg.rb`

#### TG:
* Example post 

```
I, [2016-06-09T09:54:24.664910 #45980]  INFO -- : alert           : puppet.systemerror.co.za OK server-NRPE-redhat-security  0 Security Updates Available. 107 Non-Security Updates Available Thu Jun 2 10:21:11 SAST 2016

I, [2016-06-09T09:54:24.665017 #45980]  INFO -- : posting         : https://api.telegram.org/bot123:a123/SendMessage?

```

### Global variables:
```  
  @nagios_fifo = "/var/spool/nagios/rw/ndbot.fifo"         # path to nagios named pipe
  @log         = "/var/spool/nagios/nagbot_tg.log"         # path to logfile for this daemon
  @log_size    = 1024000                                   # logfile size in kb
  @logger      = Logger.new(@log, @log_size)               # logger instance

  @tg_prot     = "https"                                   
  @tg_host     = "api.telegram.org"                    
  @tg_botid    = "bot123:a123"                                  # https://core.telegram.org/bots/api
  @tg_api      = "SendMessage?"
  @tg_chatid   = "-1234"                                   # name of chat group
  @tg_url      = "#{@tg_prot}://#{@tg_host}/#{@tg_botid}/#{@tg_api}"
  @tg_timeout  = 600
  @tg_retries  = 5 
  @nagios_fqdn = Facter.value("fqdn")
```

### Puppet:

```
= Class: nagbot_tg

  - This class deploys a daemon to post nagios alerts to telegram

== Parameters:

  - None

== Actions:

  - Installs:
   - /usr/local/sbin/nagbot_tg.rb

  - Installs:
   - /usr/local/etc/rc.d/nagbot_tg

  - Configures /etc/rc.conf:
   - nagbot_tg_enable="YES"

  - Ensures service is in a running state

== Requires:

  - service { 'nagbot_tg': 
   - require  => [ File ['/usr/local/sbin/nagbot_tg.rb'], File ['/usr/local/etc/rc.d/nagbot_tg'] ],

  - Entry in puppet/production/manifests/freebsd_nagios.pp

== Sample Usage:

  - include nagbot_tg
```

### Troubleshooting:
* Start the daemon:
  `/usr/local/etc/rc.d/nagbot_tg start`
* In a seperate console, watch the logfile: 
  `tail -f /var/spool/nagios/nagbot_tg.log`
* Check if it's running: 
  `[root@nagios03 ~/nagbot]# ps aux|grep nagbot_tg
root    90076  0.0  0.1 29712  9332   0  I     4:13PM   0:00.01 /usr/local/bin/ruby /usr/local/sbin/nagbot_tg.rb`
* Confirm if it's sending http data, run tcpdump on the  server: 
  `tcpdump -nXvvs 0 host api.telegram.org and port 443`
* You should see packets going out if you send a test alert directly to the fifo named pipe: 
  `echo "this is a CRIT message" > /var/spool/nagios/rw/ndbot.fifo`
* If are no packets, it's intermediary ACLs.
* If you do see packets outgoing but no responses inbound, verify inbound firewall rules.